package com.makzk.chip8;

import static org.lwjgl.opengl.GL11.*;

import org.lwjgl.LWJGLException;
import org.lwjgl.opengl.Display;
import org.lwjgl.opengl.DisplayMode;

public class Graphics {
	int height, width, scale;
	byte gfx[];
	
	public Graphics(int width, int height, int scale) {
		this.width = width;
		this.height = height;
		this.scale = scale;

		gfx = new byte[width * height];
	}
	
	/**
	 * Crea la ventana LWJGL para mostrar el emulador
	 */
	public void createWindow(){
		try {
			Display.setDisplayMode(new DisplayMode(width * scale, height * scale));
			Display.create();
			Display.setTitle("Chip8 Interpreter by Makzk");
			Display.sync(60);
		} catch (LWJGLException e) {
			e.printStackTrace();
			System.exit(0);
		}

		glMatrixMode(GL_PROJECTION);
		glLoadIdentity();
		glOrtho(0, width * scale, height * scale, 0, 1, -1);
		glMatrixMode(GL_MODELVIEW);
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);  
	}
	
	/**
	 * Dibuja un cuadrado en la ventanan LWJGL
	 * @param rgb El color RGB del cuadrado
	 * @param x La posición X en la ventana
	 * @param y La posición Y en la ventana
	 * @param size El tamaño de lado del cuadrado
	 */
	public void drawQuad(float[] rgb, int x, int y, int size){
		if (rgb.length != 3)
			return;

		// Set color
		glColor3f(rgb[0], rgb[1], rgb[2]);

		// Draw quad
		glBegin(GL_QUADS);
			glVertex2f(x, y);
			glVertex2f(x+size, y);
			glVertex2f(x+size, y+size);
			glVertex2f(x, y+size);
		glEnd();
		
		glFlush();
	}
	
	/**
	 * Dibuja un "pixel"
	 * @param on 1 para blanco, 0 para negro
	 * @param x La posición X en la ventana
	 * @param y La posición Y en la ventana
	 */
	public void drawPixel(int on, int x, int y) {
		if(x > width) x = 0;
		if(y > height) y = 0;
		
		drawQuad(new float[] { on, on, on }, x*scale, y*scale, scale);
	}
	public void repaintGFX(){
		for(int y = 0; y < height; y++)
			for(int x = 0; x < width; x++)
				drawPixel(gfx[(width*y+x)], x, y);
	}
	public void clearScreen(){
		gfx = new byte[width * height];
		glClearColor(0, 0, 0, 1); // Set background color to black and opaque
		glClear(GL_COLOR_BUFFER_BIT); // Clear the color buffer (background)
	}
	public void destroy() {
		if(Display.isCreated() && Display.isActive())
			Display.destroy();
	}
}
