package com.makzk.chip8;

import org.lwjgl.input.Keyboard;

public class KeyboardEmu {
	static char realInput[] = new char[]{
			Keyboard.KEY_X, // 0
			Keyboard.KEY_1, // 1
			Keyboard.KEY_2, // 2
			Keyboard.KEY_3, // 3
			Keyboard.KEY_Q, // 4
			Keyboard.KEY_W, // 5
			Keyboard.KEY_E, // 6
			Keyboard.KEY_A, // 7
			Keyboard.KEY_S, // 8
			Keyboard.KEY_D, // 9
			Keyboard.KEY_Z, // A
			Keyboard.KEY_C, // B
			Keyboard.KEY_4, // C
			Keyboard.KEY_R, // D
			Keyboard.KEY_F, // E
			Keyboard.KEY_V, // F
	};
	public static byte[] keybStatus(){
		byte status[] = new byte[16];

		for (int i = 0; i < status.length; i++) {
			status[i] = (byte) (Keyboard.isKeyDown(realInput[i]) ? 1 : 0);
		}
		return status;
	}
	
	public static boolean keyPressed(){
		return false;
	}
}
