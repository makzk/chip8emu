package com.makzk.chip8;

import java.io.File;

import javax.swing.JOptionPane;

public class Main {

	public static void main(String[] args) {
		int nFiles = 0;
		for (File x : (new File("roms/")).listFiles()) {
			if (x.isFile())
				nFiles++;
		}

		if (nFiles > 0) {
			do{
				String game = getROMPath();
	
				if(game == null)
					System.exit(0);
				
				Interpreter2 emu = new Interpreter2();
	
				emu.load(game);
				emu.start();
			} while(true); 
		} else
			JOptionPane.showMessageDialog(null, "No ROMs available", "CHIP8Emu", 0);
	}
	
	public static String getROMPath() {
		String files[] = null;
		int nFiles = 0;
		File folder = new File("roms/");
		for (File x : folder.listFiles()) {
			if (x.isFile())
				nFiles++;
		}
		files = new String[nFiles];
		int i = 0;
		for (File f : folder.listFiles()) {
			if (f.isFile()) {
				files[i] = f.getName();
				i++;
			}
		}
		Object game = JOptionPane.showInputDialog(null,
				"Choose the program to load", "Select ROM",
				JOptionPane.QUESTION_MESSAGE, null, files, files[0]);

		return (String) ((game == null) ? game : "roms/" + game);
	}

}
