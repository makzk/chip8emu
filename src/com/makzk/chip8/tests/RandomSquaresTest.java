package com.makzk.chip8.tests;

import org.lwjgl.opengl.Display;

import com.makzk.chip8.Graphics;

public class RandomSquaresTest {

	public static void main(String[] args) {
		long init = System.currentTimeMillis();
		int fps = 0;
		
		Graphics gfx = new Graphics(64, 32, 10);
		gfx.createWindow();
		
		FPSDisplay fpsw = new FPSDisplay();
		fpsw.setVisible(true);
		while (!Display.isCloseRequested()){
			int x = (int) (Math.random()*64),
				y = (int) (Math.random()*32),
				i = (int) (Math.round(Math.random()));
			gfx.drawPixel(i, x, y);
			Display.update();
			fps++;
			
			long now = System.currentTimeMillis();
			
			if((now - init) >= 100){
				fpsw.setFPS((fps*10)+"");
				fps = 0;
				init = now;
			}
		}
		
		Display.destroy();
		fpsw.dispose();
	}

}
