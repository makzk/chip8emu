package com.makzk.chip8.tests;

import static org.lwjgl.opengl.GL11.GL_COLOR_BUFFER_BIT;
import static org.lwjgl.opengl.GL11.GL_DEPTH_BUFFER_BIT;
import static org.lwjgl.opengl.GL11.glClear;

import org.lwjgl.input.Mouse;
import org.lwjgl.opengl.Display;

import com.makzk.chip8.Graphics;

public class MouseDrawTest {

	public static void main(String[] args) {
		Graphics g = new Graphics(64, 32, 10);
		g.createWindow();
		
		int lastx = -1, lasty = -1;
		while (!Display.isCloseRequested()){
			if(Mouse.isButtonDown(0)){
				int x = Mouse.getX();
				int y = (Mouse.getY()-320)*-1;
				
				if(x != lastx && y != lasty) {
					g.drawQuad(new float[]{1,1,1}, x, y, 10);
					lastx = x;
					lasty = y;
				}
			}
			if(Mouse.isButtonDown(1))
				g.clearScreen();
			
			Display.update();
			Display.sync(60);
		}
		
		Display.destroy();
	}

}
