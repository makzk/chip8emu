package com.makzk.chip8.tests;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;
import javax.swing.border.EmptyBorder;

public class FPSDisplay extends JFrame {
	private static final long serialVersionUID = 1L;
	private JPanel contentPane;
	
	private JLabel fps;

	/**
	 * Launch the application.
	 */
	public void show(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					FPSDisplay frame = new FPSDisplay();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public FPSDisplay() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 119, 73);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblFps = new JLabel("FPS:");
		lblFps.setHorizontalAlignment(SwingConstants.CENTER);
		lblFps.setBounds(5, 5, 47, 26);
		contentPane.add(lblFps);
		
		fps = new JLabel("0");
		fps.setHorizontalAlignment(SwingConstants.RIGHT);
		fps.setBounds(62, 11, 34, 14);
		contentPane.add(fps);
	}
	
	/**
	 * Set FPS
	 */
	public void setFPS(String str) {
		fps.setText(str);
	}
}
