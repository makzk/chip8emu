package com.makzk.chip8.tests;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

import com.makzk.chip8.Main;

public class ReadRomTest {
	static int MEM_SIZE = 4096;
	
	public static void main(String[] args) {
		try {
			String path = Main.getROMPath();
			byte[] rom = Files.readAllBytes(Paths.get(path));
			if(rom.length > (MEM_SIZE - 0x200)) {
				System.out.println("ROM file greater than available memory");
			} else {
				for(int i = 0; i < rom.length; i++) {
					if((i%17) == 0 && i != 0)
						System.out.println();
					System.out.print(String.format("%02X", rom[i]) + " ");
					
				}
				
			}
		} catch (IOException e) {
			System.out.println("Failed to read ROM: " + e.getMessage());
			e.printStackTrace();
		}
	}
}
