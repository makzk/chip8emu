package com.makzk.chip8;

import java.awt.Toolkit;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

import javax.swing.JOptionPane;

import org.lwjgl.opengl.Display;

public class Interpreter2 {
	byte sp, mem[] = new byte[4096], V[] = new byte[16],
			keyb[] = new byte[16];
	int WIDTH = 64, HEIGHT = 32, SCALE = 10, op, MEM_SIZE = 4096;
	
	short snd_timer, delay_timer, I, pc = 0x200, stack[] = new short[16];
	
	boolean ERROR = false, DEBUG = true, ROM_EXIT = false, REPAINT = false;
	String ERROR_MSG = "";
	
	public Graphics gfx;
	
	public Interpreter2() {
		mem = new byte[MEM_SIZE];
		if(DEBUG) {
			Debugger.main(null);
		}
		(new Font(this)).toMemory();
				
		debug("Font loaded");
	}
	
	public void start() {
		gfx = new Graphics(WIDTH, HEIGHT, SCALE);
		gfx.createWindow();
		
		while (!Display.isCloseRequested() && !ERROR && !ROM_EXIT) {
			
			// Game Loop here
			keyb = KeyboardEmu.keybStatus();
			emuCycle();
			
			if(pc < 0x200) {
				ERROR = true;
				ERROR_MSG = "Illegal memory access";
				break;
			}
			
			if (REPAINT) {
				gfx.repaintGFX();
				REPAINT = false;
			}

			if(Display.isActive())
				Display.update();
		}
				
		gfx.destroy();
		if(ERROR) {
			ERROR = false;
			JOptionPane.showMessageDialog(null, ERROR_MSG, "Error", 0);
		}
		if(DEBUG) {
			Debugger.dispose();
		}
	}
	
	public void load(String path) {
		try {
			byte[] rom = Files.readAllBytes(Paths.get(path));
			if(rom.length > (MEM_SIZE - 0x200)) {
				ERROR = true;
				ERROR_MSG = "ROM file greater than available memory";
			} else {
				for(int i = 0; i < rom.length; i++) {
					mem[i + 0x200] = rom[i];
				}
				debug("ROM Loaded");
				
			}
		} catch (IOException e) {
			ERROR = true;
			ERROR_MSG = "Failed to read ROM: " + e.getMessage();
		}
	}
	
	private void debug(String msg) {
		if(DEBUG)
			System.out.println(msg);
	}
	
	private void emuCycle() {

		int op1 = (mem[pc] & 0xFF) << 8, op2 = (mem[pc + 1] & 0xFF);
		op = op1 | op2;
		
		short dpc = pc;
		pc += 2;
		
		String detail = "";
		
		// shorthand for X and Y in opcodes
		final byte x = (byte) ((op & 0x0F00) >> 8), y = (byte) ((op & 0x00F0) >> 4);

		switch (op & 0xF000) { // decode
		// execute
		case 0x0000:
			if (op == 0x00E0) { // 00E0: CLS
				detail = "cls";
				gfx.clearScreen();
			} else if (op == 0x00EE) { // 00EE: ET
				detail = String.format("pc = 0x%03X", stack[sp]);
				pc = stack[--sp];
			} else
				detail = "unsupported";
			break;
		case 0x1000: // 1NNN: Sets pc = NNN
			int nop = (op & 0xFFF);
			detail = String.format("pc = 0x%03X", nop);
			
			// if the opcode sets this same instruction, then there will be an infinite loop
			// so we determine that the program has ended
			if(dpc == (short) nop) {
				ROM_EXIT = true;
				detail += " iloop";
			}
			pc = (short) nop;
			
			break;

		case 0x2000:
			stack[sp++] = (short) pc;
			// if the opcode sets this same instruction, then there will be an infinite loop
			// so we determine that the program has ended
			if(dpc == (op & 0x0FFF))
				ROM_EXIT = true;
			
			pc = (short) (op & 0x0FFF);
			
			detail = String.format("pc = 0x0%03X" + (ROM_EXIT?" exit":""), pc);
			break;

		case 0x3000:
			detail = String.format("V[%X] (0x%02X) == 0x%02X skip ? ", x, V[x], op & 0xFF);
			if (V[x] == (op & 0xFF)){
				detail += "yes";
				pc += 2;
			} else
				detail += "nop";
			break;

		case 0x4000:
			detail = String.format("V[%X] (0x%02X) != 0x%02X skip ? ", x, V[x], op & 0xFF);
			if (V[x] != (op & 0xFF)) {
				pc += 2;
				detail += "yes";
			} else
				detail += "nop";
			break;

		case 0x5000:
			detail = String.format("V[%X] (0x%02X) == V[%X] (%02X) skip ? ", x, V[x], y, V[y]);
			if (V[x] == V[y]) {
				pc += 2;
				detail += "yes";
			} else
				detail += "nop";
			break;

		case 0x6000: // 6XNN V[X] = NN
			detail = String.format("V[%X] = 0x%02X", x, (op & 0x00FF));
			V[x] = (byte) (op & 0x00FF);
			break;

		case 0x7000:
			detail = String.format("V[%X] += 0x%02X", x, (op & 0x00FF));
			V[x] += op & 0x00FF;
			break;

		case 0x8000:
			switch (op & 0x000F) {
			case 0x0: // 0x8XY0: V[X] = V[Y]
				V[x] = V[y];
				detail = String.format("V[%X] = V[%X] (%02X)", x, y, V[y]);
				break;

			case 0x1:
				detail = String.format("V[%X](%02X) |= V[%X] (%02X)", x, V[x], y, V[y]);
				V[x] |= V[y];
				break;

			case 0x2:
				detail = String.format("V[%X](%02X) &= V[%X] (%02X)", x, V[x], y, V[y]);
				V[x] &= V[y];
				break;
			case 0x3:
				detail = String.format("V[%X](%02X) ^= V[%X] (%02X)", x, V[x], y, V[y]);
				V[x] ^= V[y];
				break;
			case 0x4:
				V[0xF] = (byte) ((V[y] > (0xFF - V[x])) ? 1 : 0);
				V[x] += V[y];
				break;
			case 0x5:
				V[0xF] = (byte) ((V[y] > V[x]) ? 0 : 1);					
				V[x] -= V[y];
				break;

			case 0x6:
				V[0xF] = (byte) (V[x] & 0x1);
				V[x] >>= 1;
				break;

			case 0x7:
				V[0xF] = (byte) ((V[x] > V[y]) ? 0 : 1);
				V[x] = (byte) (V[y] - V[x]);				
				break;

			case 0xE:
				V[0xF] = (byte) (V[x] >> 7);
				V[x] <<= 1;
				break;
			default:
				detail = "unknown";
			}
			break;

		case 0x9000: // 9XY0: skip if V[X] != V[Y]
			if (V[x] != V[y]) {
				detail = "skip";
				pc += 2;
			} else
				detail = "nothing";
			break;

		case 0xA000: // ANNN: Sets I to the address NNN
			I = (short) (op & 0x0FFF);
			detail = String.format("I = 0x%03X", I);
			break;

		case 0xB000: // BNNN: Sets pc = NNN + V[0]
			pc = (short) ((op & 0x0FFF) + V[0]);
			detail = String.format("pc = 0x%04X", pc);
			break;

		case 0xC000: // CXNN: V[X] = random(0, 255) & NN
			V[x] = (byte) ((int) (Math.random() * 255) & (op & 0x00FF));
			break;

		case 0xD000:
			// DXYN
			V[0xF] = 0;
			
			int yl = 0, xl = 0;
			try {
				for (yl = 0; yl < (op & 0xF); yl++) {
					for (xl = 0; xl < 8; xl++) {
						if ((mem[I + yl] & (0x80 >> xl)) != 0) {
							debug(String.format("paint: V[%X] = 0x%02X, V[%X] = 0x%02X, xl = " + xl +", yl = " + yl, x, V[x], y, V[y]));
							if (gfx.gfx[(V[x] + xl + ((V[y] + yl) * WIDTH))] == 1) 
								V[0xF] = 1;
							gfx.gfx[V[x] + xl + ((V[y] + yl) * WIDTH)] ^= 1;
						}
					}
				}
				REPAINT = true;
			} catch(Exception e) {
				ERROR = true;
				ERROR_MSG = "Error trying to print sprite\n";
				ERROR_MSG += String.format("xl = %s, yl = %s, V[%X] = 0x%02X, V[%X] = 0x%02X, I = 0x%03X", xl, yl, x, V[x], y, V[y], I);
				e.printStackTrace();
				gfx.destroy();
			}
			detail = "paint";

			break;

		case 0xE000:
			switch (op & 0x00FF) {
			case 0x009E:
				if (keyb[V[x]] != 0){
					pc += 2;
					detail = String.format("key %X pressed -> skip", V[x]);
				} else
					detail = String.format("key %X not pressed -> nothing", V[x]);
				break;

			case 0x00A1:
				if (keyb[V[x]] == 0){
					pc += 2;
					detail = String.format("key %X not pressed -> skip", V[x]);
				} else

					detail = String.format("key %X pressed -> nothing", V[x]);
				break;

			default:
				detail = "unknown";
			}
			break;

		case 0xF000:
			switch (op & 0xFF) {
			case 0x07:
				V[x] = (byte) delay_timer;
				break;

			case 0x0A:
				boolean keyPress = false;
				for (int i = 0; i < 16; i++) {
					if (keyb[i] != 0) {
						V[x] = (byte) i;
						keyPress = true;
						break;
					}
				}
				if (!keyPress) {
					detail = "wait keypress";
					pc -= 2;
				}
				break;

			case 0x15:
				delay_timer = V[x];
				break;

			case 0x18:
				snd_timer = V[x];
				break;

			case 0x1E:
				V[0xF] = (byte) ((I + V[x]) > 0xFFF ? 1 : 0);
				detail = String.format("V[F] = %X, I(0x%03X) += V[%X](0x%02X)", V[0xF], I, x, V[x]);
				I += V[x];
				break;

			case 0x29: // FX29: I = location of sprite (font) for digit V[X].
				I = (short) (V[x] * 0x5);
				detail = String.format("I = V[%X] * 0x5 = 0x%04X", V[x], I);
				break;

			case 0x33:
				mem[I]     = (byte) (V[x] / 100);
				mem[I + 1] = (byte) ((V[x] / 10) % 10);
				mem[I + 2] = (byte) ((V[x] % 100) % 10);	
				detail = "V[%X]: 0x%02X, M0x%04X = 0x%02X M0x%04X = 0x%02X M0x%04X = 0x%02X";
				detail = String.format(detail, x, V[x], I, mem[I], I + 1,
						mem[I + 1], I + 2, mem[I + 2]);
				break;

			case 0x55:
				for (int i = 0; i < x; i++) {
					mem[I + i] = V[i];
					detail += String.format("M0x%04X = V[0x%X] ", I + i, V[i]);
				}
				detail += String.format("I (0x%03X) += %X+1 ", I, x);
				I += x + 1;
				break;

			case 0x65:
				I += x + 1;
				detail = String.format("I += %X + 1 = 0x%03X", x, I);
				for (int i = 0; i < x; i++) {
					detail += String.format("V[%X] = M0x%04X ", i, mem[I + i]);
					V[i] = mem[I + i];
				}
				break;
			default:
				detail = "unsupported";
			}
			break;
		default:
			detail = "unsupported";
		}

		String msg = String.format("PC:0x%04X OP:0x%04X", dpc, op);

		if (!detail.equals(""))
			msg += " " + detail;
		debug(msg);

		if(DEBUG) {
			Debugger.setV(V);
			Debugger.setPC(dpc);
			Debugger.setOP(op);
		}
		
		if((pc & 0xFFF) < 0x200) {
			ERROR = true;
			ERROR_MSG = String.format("Illegal access to memory (0x%03X)", pc);
			return;
		}
		

		// Update timers
		if (delay_timer > 0)
			delay_timer--;

		if (snd_timer > 0) {
			if (snd_timer == 1)
				Toolkit.getDefaultToolkit().beep();
			snd_timer--;
		}
	}
}
