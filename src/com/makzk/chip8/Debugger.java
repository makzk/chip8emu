package com.makzk.chip8;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextArea;
import javax.swing.JTextField;

public class Debugger {

	private JFrame frame;
	private JTextField V0;
	private JTextField V1;
	private JTextField V2;
	private JTextField V3;
	private JTextField V4;
	private JTextField V5;
	private JTextField V6;
	private JTextField V7;
	private JTextField V8;
	private JTextField V9;
	private JTextField VA;
	private JTextField VB;
	private JTextField VC;
	private JTextField VD;
	private JTextField VE;
	private JTextField VF;
	private JTextField PC;
	private JTextField OP;
	
	public static Debugger window;
	private JTextArea debugText;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					window = new Debugger();
					window.frame.setVisible(true);
					window.frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public Debugger() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 460, 300);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		V0 = new JTextField();
		V0.setEditable(false);
		V0.setBounds(30, 11, 45, 20);
		frame.getContentPane().add(V0);
		V0.setColumns(10);
		
		V1 = new JTextField();
		V1.setEditable(false);
		V1.setBounds(30, 42, 45, 20);
		frame.getContentPane().add(V1);
		V1.setColumns(10);
		
		V2 = new JTextField();
		V2.setEditable(false);
		V2.setBounds(30, 73, 45, 20);
		frame.getContentPane().add(V2);
		V2.setColumns(10);
		
		V3 = new JTextField();
		V3.setEditable(false);
		V3.setBounds(30, 104, 45, 20);
		frame.getContentPane().add(V3);
		V3.setColumns(10);
		
		V4 = new JTextField();
		V4.setEditable(false);
		V4.setBounds(30, 135, 45, 20);
		frame.getContentPane().add(V4);
		V4.setColumns(10);
		
		V5 = new JTextField();
		V5.setEditable(false);
		V5.setBounds(30, 163, 45, 20);
		frame.getContentPane().add(V5);
		V5.setColumns(10);
		
		JLabel lblV = new JLabel("V0");
		lblV.setBounds(10, 14, 22, 14);
		frame.getContentPane().add(lblV);
		
		JLabel lblV_1 = new JLabel("V1");
		lblV_1.setBounds(10, 42, 22, 14);
		frame.getContentPane().add(lblV_1);
		
		JLabel lblV_2 = new JLabel("V2");
		lblV_2.setBounds(10, 73, 22, 14);
		frame.getContentPane().add(lblV_2);
		
		JLabel lblV_3 = new JLabel("V3");
		lblV_3.setBounds(10, 104, 22, 14);
		frame.getContentPane().add(lblV_3);
		
		JLabel lblV_4 = new JLabel("V4");
		lblV_4.setBounds(10, 135, 22, 14);
		frame.getContentPane().add(lblV_4);
		
		JLabel lblV_5 = new JLabel("V5");
		lblV_5.setBounds(10, 166, 22, 14);
		frame.getContentPane().add(lblV_5);
		
		V6 = new JTextField();
		V6.setEditable(false);
		V6.setBounds(30, 195, 45, 20);
		frame.getContentPane().add(V6);
		V6.setColumns(10);
		
		V7 = new JTextField();
		V7.setEditable(false);
		V7.setBounds(30, 226, 45, 20);
		frame.getContentPane().add(V7);
		V7.setColumns(10);
		
		JLabel lblV_6 = new JLabel("V6");
		lblV_6.setBounds(10, 197, 22, 14);
		frame.getContentPane().add(lblV_6);
		
		JLabel lblNewLabel = new JLabel("V7");
		lblNewLabel.setBounds(10, 229, 22, 14);
		frame.getContentPane().add(lblNewLabel);
		
		JLabel lblV_7 = new JLabel("V8");
		lblV_7.setBounds(85, 14, 22, 14);
		frame.getContentPane().add(lblV_7);
		
		V8 = new JTextField();
		V8.setEditable(false);
		V8.setColumns(10);
		V8.setBounds(105, 11, 45, 20);
		frame.getContentPane().add(V8);
		
		JLabel lblVa = new JLabel("V9");
		lblVa.setBounds(85, 42, 22, 14);
		frame.getContentPane().add(lblVa);
		
		V9 = new JTextField();
		V9.setEditable(false);
		V9.setColumns(10);
		V9.setBounds(105, 42, 45, 20);
		frame.getContentPane().add(V9);
		
		JLabel lblVb = new JLabel("VA");
		lblVb.setBounds(85, 73, 22, 14);
		frame.getContentPane().add(lblVb);
		
		VA = new JTextField();
		VA.setEditable(false);
		VA.setColumns(10);
		VA.setBounds(105, 73, 45, 20);
		frame.getContentPane().add(VA);
		
		JLabel lblVc = new JLabel("VB");
		lblVc.setBounds(85, 104, 22, 14);
		frame.getContentPane().add(lblVc);
		
		VB = new JTextField();
		VB.setEditable(false);
		VB.setColumns(10);
		VB.setBounds(105, 104, 45, 20);
		frame.getContentPane().add(VB);
		
		JLabel lblVd = new JLabel("VC");
		lblVd.setBounds(85, 135, 22, 14);
		frame.getContentPane().add(lblVd);
		
		VC = new JTextField();
		VC.setEditable(false);
		VC.setColumns(10);
		VC.setBounds(105, 135, 45, 20);
		frame.getContentPane().add(VC);
		
		JLabel lblVe = new JLabel("VD");
		lblVe.setBounds(85, 166, 22, 14);
		frame.getContentPane().add(lblVe);
		
		VD = new JTextField();
		VD.setEditable(false);
		VD.setColumns(10);
		VD.setBounds(105, 163, 45, 20);
		frame.getContentPane().add(VD);
		
		JLabel lblVe_1 = new JLabel("VE");
		lblVe_1.setBounds(85, 197, 22, 14);
		frame.getContentPane().add(lblVe_1);
		
		VE = new JTextField();
		VE.setEditable(false);
		VE.setColumns(10);
		VE.setBounds(105, 195, 45, 20);
		frame.getContentPane().add(VE);
		
		JLabel lblV_8 = new JLabel("VF");
		lblV_8.setBounds(85, 229, 22, 14);
		frame.getContentPane().add(lblV_8);
		
		VF = new JTextField();
		VF.setEditable(false);
		VF.setColumns(10);
		VF.setBounds(105, 226, 45, 20);
		frame.getContentPane().add(VF);
		
		JLabel lblPc = new JLabel("PC");
		lblPc.setBounds(160, 14, 22, 14);
		frame.getContentPane().add(lblPc);
		
		PC = new JTextField();
		PC.setEditable(false);
		PC.setBounds(180, 11, 54, 20);
		frame.getContentPane().add(PC);
		PC.setColumns(10);
		
		JLabel lblOpcode = new JLabel("Opcode");
		lblOpcode.setBounds(244, 14, 46, 14);
		frame.getContentPane().add(lblOpcode);
		
		OP = new JTextField();
		OP.setEditable(false);
		OP.setBounds(289, 11, 54, 20);
		frame.getContentPane().add(OP);
		OP.setColumns(10);
		
		debugText = new JTextArea();
		debugText.setEditable(false);
		debugText.setEnabled(false);
		debugText.setBounds(160, 40, 274, 211);
		frame.getContentPane().add(debugText);
	}

	public static void dispose() {
		window.frame.dispose();
	}
	public static void setV(byte V[]) {
		window.V0.setText(String.format("0x%02X", V[0]));
		window.V1.setText(String.format("0x%02X", V[1]));
		window.V2.setText(String.format("0x%02X", V[2]));
		window.V3.setText(String.format("0x%02X", V[3]));
		window.V4.setText(String.format("0x%02X", V[4]));
		window.V5.setText(String.format("0x%02X", V[5]));
		window.V6.setText(String.format("0x%02X", V[6]));
		window.V7.setText(String.format("0x%02X", V[7]));
		window.V8.setText(String.format("0x%02X", V[8]));
		window.V9.setText(String.format("0x%02X", V[9]));
		window.VA.setText(String.format("0x%02X", V[0xA]));
		window.VB.setText(String.format("0x%02X", V[0xB]));
		window.VC.setText(String.format("0x%02X", V[0xC]));
		window.VD.setText(String.format("0x%02X", V[0xD]));
		window.VE.setText(String.format("0x%02X", V[0xE]));
		window.VF.setText(String.format("0x%02X", V[0xF]));
	}
	public static void setPC(short pc) {
		window.PC.setText(String.format("0x%02X", pc));
	}
	public static void setOP(int op) {
		window.OP.setText(String.format("0x%04X", op));
	}
	public static void addDebugText(String msg) {
		if(window != null)
			window.debugText.setText(
				window.debugText.getText() + "\n" + msg);
		else
			System.out.println("debug: " + msg);
	}
	public static boolean isActive() {
		return window.frame.isActive();
	}
}
